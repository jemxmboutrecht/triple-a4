<?php
include BASEDIR.'application/function/product-functions.php';
// Main function
function page_content($page) {
    if (empty($page['url']['pagevars'])) {
        $overview = product_overview();
        return $overview;
    } else {
        switch($page['url']['pagevars'][0]) {
            case 'category':
                $overview = category_overview(array_slice($page['url']['pagevars'], 1));
                return $overview;
                break;
            case 'product':
                $product = get_products_by_id(array_slice($page['url']['pagevars'], 1));
                return array('product' => $product);
                break;
        }
    }
}

// Functions
function product_overview() {
    $categories = get_categories();
    $products = get_products();

    return array('categories' => $categories, 'products' => $products);
}

function category_overview($categoriesArr) {
    $categories = get_categories($categoriesArr);
    $products = get_products_by_category($categoriesArr);

    return array('categories' => $categories, 'products' => $products);
}

// Helpers
function short_description($description) {
    return preg_replace('/\s+?(\S+)?$/', '... ', substr($description, 0, 50));
}

function format_price($price) {
    $priceArr = explode('.', $price);
    $decimals = str_replace('0.', ',', number_format(floatval('.'.$priceArr[1]), 2));
    return '<h2>'.$priceArr[0].'</h2><sup>'.$decimals.'</sup>';
}