<?php

require_once BASEDIR.'application/function/user-functions.php';
require_once BASEDIR.'application/function/cart-functions.php';
require_once BASEDIR.'application/function/checkout-functions.php';

function page_content($page) {

  if (isset($page['url']['pagevars']) && !empty($page['url']['pagevars'])) {

    if ($page['url']['pagevars'][0] == 'cart' && !empty($page['url']['pagevars'][1])) {

      $user = check_user_session();

      if (isset($page['url']['pagevars'][2]) && $page['url']['pagevars'][2] == 'status') {
        if ((int)$page['url']['pagevars'][3] > 0) {
          $statusId = (int)$page['url']['pagevars'][3];
        } else {
          switch ($page['url']['pagevars'][3]) {
            case 'success':
              $statusId = 2;
              break;
            case 'hold':
              $statusId = 4;
              break;
            case 'cancel':
              $statusId = 3;
              break;
          }
        }
      } else {
        $statusId = 1;
      }

      $cart = load_shoppingcart(false, $page['url']['pagevars'][1]);
      //var_dump($statusId); exit;
      $order = set_order((int)$user['user_id'], $cart, $statusId);

    }
  }

  return array('order'=>$order, 'cart'=>$cart, 'user' => $user);
}
