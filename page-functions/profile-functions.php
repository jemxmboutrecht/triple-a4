<?php

function page_content($page) {
    if (isset($page['url']['pagevars'][0]) && $page['url']['pagevars'][0] == 'orders') {
        $orders = get_user_orders();
        return array('orders' => $orders);
    }
}

function get_user_orders() {
    require_once BASEDIR.'application/function/user-functions.php';
    require_once BASEDIR.'application/function/cart-functions.php';
    require_once BASEDIR.'application/function/checkout-functions.php';

    $user = check_user_session();

    return get_orders((int)$user['user_id']);
}