<?php

function page_content($page) {
    if (check_user_session() !== false) {
        header('Location:'.$page['url']['baseurl'].'profile');
    }
    if (isset($_POST['login'])) {
        return array('loginfeedback' => login());
    }
}