<?php
require_once 'application/application.php';
$page = start_application();

require_once 'application/function/menu-functions.php';
$pageOrder = get_structure();

require_once 'application/function/user-functions.php';

$content = null;
if (page_functions($page)) {
    $content = page_content($page);
}

?>
<!DOCTYPE html>
<html lang="en">
<head id="tophead">

    <title>Triple A4 | <?=$page['url']['pagename']?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=$page['url']['baseurl']?>lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$page['url']['baseurl']?>lib/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=$page['url']['baseurl']?>lib/css/main.css">
    <!-- Styling by: Jesse Malotaux | JEMX - MBO Utrecht <jemx@mboutrecht.nl> -->
    <link rel="stylesheet" href="<?=$page['url']['baseurl']?>lib/css/jemxstyling.css">
    <script>baseUrl = "<?=$page['url']['baseurl']?>";</script>

</head>
<body>

<div id="mainContent">

    <?php include 'parts/main_menu.phtml'; ?>

    <?php load_page_template($page, $content); ?>

</div>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?=$page['url']['baseurl']?>lib/js/inc/jquery-3.2.1.min.js"></script>
<script src="<?=$page['url']['baseurl']?>lib/js/inc/popper.min.js"></script>
<script src="<?=$page['url']['baseurl']?>lib/js/inc/bootstrap.min.js"></script>

<?=page_script($page);?>

<script src="<?=$page['url']['baseurl']?>lib/js/script.js"></script>

</body>
</html>
