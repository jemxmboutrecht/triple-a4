-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 06, 2020 at 03:20 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `triplea4`
--

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

DROP TABLE IF EXISTS `carousel`;
CREATE TABLE IF NOT EXISTS `carousel` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`item_id`, `title`, `content`, `image`) VALUES
(1, 'Welkom!', 'Welkom op de website van Triple A4. ', 'carousel-1.jpg'),
(2, 'Registreren', 'Om snel van start te gaan moet u eerst registreren en daarna kan u bestellen.', 'carousel-2.jpg'),
(3, 'Winkel', 'Om te bestellen moet u eerst producten aan uw winkelwagen toevoegen. ', 'carousel-3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `products` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `cart_opened` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cart_closed` datetime DEFAULT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`cart_id`, `products`, `cart_opened`, `cart_closed`) VALUES
(1, NULL, '2020-03-06 15:58:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orderlines`
--

DROP TABLE IF EXISTS `orderlines`;
CREATE TABLE IF NOT EXISTS `orderlines` (
  `orderline_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` int(3) NOT NULL DEFAULT '1',
  `discount` float NOT NULL,
  `price` float NOT NULL,
  `price_total` float NOT NULL,
  PRIMARY KEY (`orderline_id`),
  KEY `orderid` (`order_id`),
  KEY `productid` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_price_ex` float NOT NULL,
  `order_price_inc` float NOT NULL,
  `order_status_id` int(2) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `userid` (`user_id`),
  KEY `order_status_id` (`order_status_id`),
  KEY `cart_id` (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
CREATE TABLE IF NOT EXISTS `order_status` (
  `status_id` int(2) NOT NULL AUTO_INCREMENT,
  `status_code` varchar(50) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`status_id`, `status_code`) VALUES
(1, 'open'),
(2, 'payed'),
(3, 'canceled'),
(4, 'on hold');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `page_order` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent_id` (`parent_id`,`page_order`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `page_url`, `parent_id`, `page_order`) VALUES
(1, 'Home', 'home', NULL, 0),
(2, 'Winkel', 'shop', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_content`
--

DROP TABLE IF EXISTS `page_content`;
CREATE TABLE IF NOT EXISTS `page_content` (
  `page_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `page_content_order` int(3) NOT NULL,
  `content_title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`page_content_id`),
  KEY `page_id` (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_content`
--

INSERT INTO `page_content` (`page_content_id`, `page_id`, `page_content_order`, `content_title`, `content`) VALUES
(1, 1, 0, 'Welkom', 'Welkom op deze pagina. <br>\r\n<br>\r\nDit is de home pagina.'),
(2, 1, 1, 'Welkom 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac maximus nisi, ut dapibus arcu. Aliquam erat volutpat. Praesent ornare ac lorem in rutrum. Nullam ultricies nisi purus, interdum semper urna venenatis eu. Morbi fringilla pellentesque consequat. Nunc diam diam, eleifend vitae congue at, volutpat ac velit. Sed feugiat sapien sit amet justo accumsan maximus. Cras congue neque in mauris mattis faucibus. Phasellus facilisis quam vel enim vestibulum, ut pellentesque elit porta. Phasellus tempus lacinia ante, vitae porta nisi consequat ac. Maecenas vitae nunc metus. Morbi rutrum ac nulla sed condimentum.'),
(3, 1, 2, 'Super actie', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac maximus nisi, ut dapibus arcu. Aliquam erat volutpat. Praesent ornare ac lorem in rutrum. Nullam ultricies nisi purus, interdum semper urna venenatis eu. Morbi fringilla pellentesque consequat. Nunc diam diam, eleifend vitae congue at, volutpat ac velit. Sed feugiat sapien sit amet justo accumsan maximus. Cras congue neque in mauris mattis faucibus. Phasellus facilisis quam vel enim vestibulum, ut pellentesque elit porta. Phasellus tempus lacinia ante, vitae porta nisi consequat ac. Maecenas vitae nunc metus. Morbi rutrum ac nulla sed condimentum.');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` bigint(80) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryid` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `category_id`, `title`, `description`, `price`, `image`) VALUES
(1, 8711744024659, 4, 'Toetsenbord 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur velit ex, venenatis non semper sed, imperdiet at felis. Vivamus blandit maximus convallis. Vestibulum eget congue mauris. Etiam bibendum vestibulum purus, eget feugiat neque ultricies vel. Nulla mollis erat at dui volutpat placerat. Praesent eget porttitor lacus. Phasellus eu quam semper, efficitur tortor ac, venenatis mauris. Vestibulum et tellus quis sapien sagittis fermentum. Curabitur in dignissim nibh. Integer ipsum leo, pulvinar ut metus rhoncus, rutrum sollicitudin libero. Nunc nec aliquet metus, quis maximus leo. Aenean eget sem nec tortor efficitur tincidunt at sagittis ex.', 27.48, 'toetsenbord1.jpg'),
(5, 20490522, 4, 'Toetsenbord 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur velit ex, venenatis non semper sed, imperdiet at felis. Vivamus blandit maximus convallis. Vestibulum eget congue mauris. Etiam bibendum vestibulum purus, eget feugiat neque ultricies vel. Nulla mollis erat at dui volutpat placerat. Praesent eget porttitor lacus. Phasellus eu quam semper, efficitur tortor ac, venenatis mauris. Vestibulum et tellus quis sapien sagittis fermentum. Curabitur in dignissim nibh. Integer ipsum leo, pulvinar ut metus rhoncus, rutrum sollicitudin libero. Nunc nec aliquet metus, quis maximus leo. Aenean eget sem nec tortor efficitur tincidunt at sagittis ex.', 42.1, 'toetsenbord2.jpg'),
(6, 8718309480172, 1, 'Computer beugel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur velit ex, venenatis non semper sed, imperdiet at felis. Vivamus blandit maximus convallis. Vestibulum eget congue mauris. Etiam bibendum vestibulum purus, eget feugiat neque ultricies vel. Nulla mollis erat at dui volutpat placerat. Praesent eget porttitor lacus. Phasellus eu quam semper, efficitur tortor ac, venenatis mauris. Vestibulum et tellus quis sapien sagittis fermentum. Curabitur in dignissim nibh. Integer ipsum leo, pulvinar ut metus rhoncus, rutrum sollicitudin libero. Nunc nec aliquet metus, quis maximus leo. Aenean eget sem nec tortor efficitur tincidunt at sagittis ex.', 36.86, 'computer-beugel.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `parent_id`, `title`, `url`, `description`) VALUES
(1, NULL, 'Standaarden', 'standaarden', ''),
(2, NULL, 'Randapparatuur', 'randapparatuur', ''),
(3, 2, 'Muizen', 'muizen', ''),
(4, 2, 'Toetsenborden', 'toetsenborden', '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `firstname` varbinary(116) NOT NULL,
  `lastname` varbinary(116) NOT NULL,
  `email` varbinary(271) NOT NULL,
  `street` varbinary(116) NOT NULL,
  `housenumber` varbinary(26) NOT NULL,
  `city` varbinary(166) NOT NULL,
  `postal` varbinary(23) NOT NULL,
  `role` int(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `lastname` (`lastname`),
  KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `firstname`, `lastname`, `email`, `street`, `housenumber`, `city`, `postal`, `role`) VALUES
(3, 'admin', 0x8a98b2a79861d17569020ed8bcc24f0a, 0x872e9f095c3e06f887ce746a1fb87f3e, 0x372f4c2b99a8353b58f09c73d2e129b84cd2ee0b743c5ddfd8f5666acdadd919, 0x5174fee81839e98c4d701b851486d97b, 0xbbbbb9c560430b64237e16cba9834cf1, 0xfd8d19a12eaf0a45b1fb8a6698564fdc, 0x33353236204550, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_cart`
--

DROP TABLE IF EXISTS `user_cart`;
CREATE TABLE IF NOT EXISTS `user_cart` (
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`cart_id`) USING BTREE,
  KEY `cart_id` (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_cart`
--

INSERT INTO `user_cart` (`user_id`, `cart_id`) VALUES
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_pass`
--

DROP TABLE IF EXISTS `user_pass`;
CREATE TABLE IF NOT EXISTS `user_pass` (
  `user_id` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_pass`
--

INSERT INTO `user_pass` (`user_id`, `password`) VALUES
(3, '8d847e01d22baa969f71fa362b4de21c9e13c7882bcea13ba5c6a8ae0d71fc8c9700c82e0087a65c8b37bd29f536747f28c9672bec1cae7762d2c9f36b6013f2');

-- --------------------------------------------------------

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
CREATE TABLE IF NOT EXISTS `user_sessions` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_ip` varchar(16) NOT NULL,
  `session_hash` varchar(32) NOT NULL,
  `session_start` timestamp NOT NULL,
  `session_end` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`session_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sessions`
--

INSERT INTO `user_sessions` (`session_id`, `user_id`, `user_ip`, `session_hash`, `session_start`, `session_end`) VALUES
(1, 3, '::1', 'd8e71721ef24bd01010a4151ffcd72ea', '2020-03-06 14:58:15', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderlines`
--
ALTER TABLE `orderlines`
  ADD CONSTRAINT `orderlines_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orderlines_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`status_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `page_content`
--
ALTER TABLE `page_content`
  ADD CONSTRAINT `page_content_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `users` (`role`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_cart`
--
ALTER TABLE `user_cart`
  ADD CONSTRAINT `user_cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_cart_ibfk_2` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_pass`
--
ALTER TABLE `user_pass`
  ADD CONSTRAINT `user_pass_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_sessions`
--
ALTER TABLE `user_sessions`
  ADD CONSTRAINT `user_sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
