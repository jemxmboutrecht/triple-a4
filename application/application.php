<?php


/**
 * Main application functions
 * Author: Jesse Malotaux <jemx@mboutrecht>
 */

function start_application()
{
  $urlArr = url_parse();

  require_once BASEDIR.'application/inc/db.php';

  if ($urlArr['pagename'] == 'application' && $urlArr['pagevars'][0] == 'function') {
    include BASEDIR . 'application/function/' . $urlArr['pagevars'][1] . '-functions.php';
    if (count($urlArr['pagevars']) < 3) {
      $urlArr['pagevars'][2]();
    } else {
      $urlArr['pagevars'][2](array_slice($urlArr['pagevars'], 3));
    }
  } else {
    $contentArr = get_pagecontent($urlArr);
  }

  $returnArr = array('url' => $urlArr);

  if (isset($contentArr)) {
    $returnArr['content'] = $contentArr;
  }

  return $returnArr;

}

/**
 *
 */
function url_parse()
{
  $baseDir = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
  $baseDir = (strpos($baseDir, 'application') !== false ? str_replace('application/', '', $baseDir) : $baseDir);
  $baseUrl = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/';

  define('BASEDIR', $baseDir);

  $varStr = str_replace($baseUrl, '', (isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $_SERVER['REQUEST_URI']));

  $urlVars = array_values(array_filter((strpos($varStr, '/') !== false ? explode('/', $varStr) : array($varStr))));
  $count = count($urlVars);

  $pageVars = array();

  if (strpos(end($urlVars), '.') !== false) {
    return null;
  }

  if ($count > 0) {
    for ($i = 0; $i < $count; $i++) {
      if (!empty($urlVars[$i])) {
        if ($i == 0) {
            $pageName = $urlVars[$i];
        } else {
          $pageVars[$i] = $urlVars[$i];
        }
      }
    }
  }

  $urlArr = array('baseurl' => $baseUrl);

  if (!empty($pageName)) {
    $pageVars = array_values($pageVars);
    $urlArr['pagename'] = $pageName;
      $urlArr['pagevars'] = $pageVars;
  } else {
    $urlArr['pagename'] = 'home';
      $urlArr['pagevars'] = array();
  }

  return $urlArr;

}

function get_pagecontent($urlArr)
{
  $db = new db();

  $db->query('SELECT id, title FROM pages WHERE page_url = :pagename;');
  $db->bind(':pagename', $urlArr['pagename']);
  $db->execute();
  $pageResult = $db->single();

  if (empty($pageResult)) {
    return null;
  } else {
    $db->query('SELECT content_title, content FROM page_content WHERE page_id = :pageid ORDER BY page_content_order;');
    $db->bind(':pageid', $pageResult['id']);
    $db->execute();
    $contentArray = $db->resultset();
      return $contentArray;
  }
}

function format_content($contentArr)
{
  $formatArr = array();
  foreach ($contentArr as $key => $content) {
    $formatArr[$key]['content-title'] = $content['content_title'];
    $formatArr[$key]['content'] = $content['content'];
  }
  return $formatArr;
}

function page_functions($page) {
  $file = BASEDIR . 'page-functions/'.$page['url']['pagename'].'-functions.php';
  if (file_exists($file)) {
    include $file;
    return true;
  } else {
    return false;
  }
}

function load_page_template($page, $content) {
  $file = BASEDIR . 'page-templates/'.$page['url']['pagename'].'.phtml';
  if (file_exists($file)) {
    include $file;
  }
}

function page_script($page) {
  $location = 'lib/js/pagescripts/'.$page['url']['pagename'].'script.js';
  $file = BASEDIR . $location;
  $link = $page['url']['baseurl'] . $location;
  if (file_exists($file)) {
    return '<script src="'.$link.'" id="pagescript"></script>';
  }
}
