<?php

require_once BASEDIR.'application/function/product-functions.php';

if (!isset($_SESSION)) session_start();
// Cart functions
function load_shoppingcart($closed = false, $cartId = false) {
  if (!isset($_SESSION['user'])) header('location: login');

  if (!$cartId) {
    if (!isset($_SESSION['user']['cartid'])) {
      $db = new db();
      if ($closed) {
        $sql = 'SELECT cart_id FROM user_cart WHERE user_id = :userId AND cart_closed IS NULL';
      } else {
        $sql = 'SELECT cart_id FROM user_cart WHERE user_id = :userId';
      }
      $db->query($sql);
      $db->bind(':userId', $_SESSION['user']['user_id']);
      $db->execute();
      $cartId = $db->single();

      if ($cartId == false) {
        $cartId = create_cart();
        connect_user_cart($cartId);
      } else {
        $cartId = $cartId['cart_id'];
      }

      $cart = get_cart_by_id($cartId);

      return $cart;
    } else {
      $cartId = $_SESSION['user']['cartid'];
    }
  }

  $cart = get_cart_by_id($cartId);

  return $cart;
}


function get_cart_by_id($cartId) {
  $db = new db();
  $db->query("SELECT * FROM carts WHERE cart_id = :cartId");
  $db->bind(':cartId', $cartId);
  $db->execute();
  $cart = $db->resultset();

  if ($cart !== false && !empty($cart)) {
    $cart = $cart[0];
    $formatProducts = format_products($cart['products']);
    $cart['products'] = $formatProducts['products'];
    $cart['carttotal'] = $formatProducts['carttotal'];
    return $cart;
  }
  return false;
}

function get_session_cartid() {

  if (isset($_SESSION['user']['cartid'])) {
    return $_SESSION['user']['cartid'];
  } else if (isset($_SESSION['user'])) {
    $db = new db();

    $db->query("SELECT cart_id FROM user_cart WHERE user_id = :userId");
    $db->bind(':userId', $_SESSION['user']['user_id']);
    $db->execute();
    $cartId = $db->single();

    if ($cartId !== false) {
      $_SESSION['user']['cartid'] = $cartId['cart_id'];
      return $cartId['cart_id'];
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function cart_total($products) {
  $prcEx = 0;

  foreach ($products as $product) {
    $prcEx += $product['prctotal'];
  }

  $prcInc = floatval(number_format(($prcEx * 1.21), 2,'.',''));
  return array('ex'=>$prcEx, 'inc'=>$prcInc);
}

function update_totals() {
  $cartId = get_session_cartid();


}

function empty_cart() {
  $cart = get_cart_products(null, true);
  save_products($cart['cartid'], array());
}

function close_cart($cartId) {
  $db = new db();

  $db->query('UPDATE carts SET cart_closed = NOW() WHERE cart_id = :cartId');
  $db->bind(':cartId', $cartId);
  $db->execute();
}

function create_cart() {
  $db = new db();
  $db->query("INSERT INTO carts (cart_opened) VALUES (NOW())");
  $db->execute();
  return $db->lastInsertId();
}

function connect_user_cart($cartId) {
  if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $db = new db();

    $db->query("SELECT cart_id FROM user_cart WHERE user_id = :userId");
    $db->bind(':userId', $user['user_id']);
    $db->execute();
    $dbCart = $db->single();

    $connectQuery = ($dbCart !== false ? 'UPDATE user_cart SET cart_id = :cartId WHERE user_id = :userId' : 'INSERT INTO user_cart (user_id, cart_id) VALUES(:userId, :cartId)');

    $db->query($connectQuery);
    $db->bind(':userId', $user['user_id']);
    $db->bind(':cartId', $cartId);
    $db->execute();
  }
}
// END cart functions

// Product functions
function add() {
  $cart = get_cart_products(null, true);
  $products = $cart['products'];
  $prdId = $_POST['productid'];
  $prc = floatval($_POST['price']);
  $amt = (int)$_POST['amount'];

  if (is_array($products) && isset($products[$prdId])) {
    $cartPrd = $products[$prdId];
    $newAmount = (int)$cartPrd['amt'] + $amt;
    $newPrice = $prc * $newAmount;
    $prdArr = array('prc' => $prc, 'prctotal' => $newPrice, 'amt' => $newAmount);
  } else {
    $prdArr = array('prc' => $prc, 'prctotal'=>$prc, 'amt'=>$amt);
  }

  $products = is_array($products) ? $products : array();
  $products[$prdId] = $prdArr;

  save_products($cart['cartid'], $products);
}

function delete() {
  $cart = get_cart_products(null, true);
  $prdId = (isset($_POST['productid']) ? $_POST['productid'] : 0);

  if (isset($cart['products']) && count($cart['products']) > 0 && $prdId != 0) {
    foreach ($cart['products'] as $key => $product) {
      if ($key == $prdId) {
        unset($cart['products'][$key]);
      }
    }
  }

  save_products($cart['cartid'], $cart['products']);
}

function get_cart_products($cartId = null, $array = false) {
  $cartId = ($cartId == null ? get_session_cartid() : $cartId);

  if (isset($_SESSION['user'])) {
    if ($cartId == false) {
      $cartId = create_cart();
      connect_user_cart($cartId);
    }

    $db = new db();

    $db->query('SELECT products, cart_closed FROM carts WHERE cart_id = :cartId');
    $db->bind(':cartId', (int)$cartId);
    $db->execute();
    $cart = $db->resultset();

    if ($cart !== false) {

      if ($cart[0]['cart_closed'] == null) {
        $products = json_decode($cart[0]['products'], true);
      } else {
        $cartId = create_cart();
        connect_user_cart($cartId);

        $_SESSION['user']['cartid'] = $cartId;

        $products = array();
      }
    }

    if (!isset($_POST) || $array) {
      return array('cartid' => $cartId, 'products' => $products);
    } else {
      echo json_encode(array('cartid' => (int)$cartId, 'products' => $products));
    }
  } else {
    if (isset($_POST)) {
      echo 0;
    } else {
      return false;
    }
  }
}

function save_products($cartId, $products) {
  $db = new db();
  $db->query("UPDATE carts SET products=:products WHERE cart_id = :cartId");
  $db->bind(':products', json_encode($products));
  $db->bind(':cartId', $cartId);
  if ($db->execute()) {
    echo 1;
  } else {
    echo 0;
  }
}

function format_products($jsonStr) {
  $prdArr = json_decode($jsonStr, true);
  if (!empty($prdArr)) {
    $products = get_products_by_id(array_keys($prdArr));

    foreach ($products as $product) {
      $prdArr[$product['id']]['info'] = $product;
    }

    $cartTotal = cart_total($prdArr);

    return array('products' => $prdArr, 'carttotal' => $cartTotal);
  } else {
    return false;
  }
}

function format_price($price) {
  $priceArr = explode('.', $price);
  if (isset($priceArr[1])) {
    $decimals = str_replace('0.', ',', number_format(floatval('.' . $priceArr[1]), 2));
  } else {
    $decimals = ',00';
  }
  return $priceArr[0].$decimals;
}
// END Product functions
