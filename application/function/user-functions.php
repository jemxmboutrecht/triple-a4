<?php

session_start();

if (isset($_COOKIE['user']) && !isset($_SESSION['user'])) {
    check_user_cookie();
}

/*
 * User functions
 * Author: Jesse Malotaux <jemx@mboutrecht.nl>
 */

function gen_userlinks($page) {
    $user = check_user_session();
    if ($user) {
        $html = '<p>Welkom '.$user['firstname'].' '.$user['lastname'].'</p>';
        $html .= '<li class="nav-item"><a class="nav-link" href="'.$page['url']['baseurl'].'profile"><i class="fa fa-user" aria-hidden="true"></i>Profiel</a></li>';
        $html .= '<li class="nav-item"><a class="nav-link" href="'.$page['url']['baseurl'].'shoppingcart"><div id="cartAmount"></div><i class="fa fa-shopping-cart" aria-hidden="true"></i>Winkelwagen</a></li>';
        $html .= '<li class="nav-item"><a class="nav-link" href="'.$page['url']['baseurl'].'logout"><i class="fa fa-power-off" aria-hidden="true"></i>Uitloggen</a></li>';
    } else {
        $html = '<li class="nav-item"><a class="nav-link" href="'.$page['url']['baseurl'].'login"><i class="fa fa-sign-in" aria-hidden="true"></i>Inloggen</a></li>';
        $html .= '<li class="nav-item"><a class="nav-link" href="'.$page['url']['baseurl'].'register"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Registreren</a></li>';
    }
    return $html;
}

function check_user_info($identifier = 'username', $value) {

    $db = new db();
    if ($identifier == 'username') {
        $userQuery = "SELECT user_id FROM users WHERE username = :val";
    } else if ($identifier == 'email') {
        $userQuery = "SELECT user_id FROM users WHERE email = AES_ENCRYPT(:val, '".$db->aes."')";
    }

    $db->query($userQuery);
    $db->bind(':val', $value);
    $db->execute();
    $checkResult = $db->single();

    if (isset($checkResult['user_id'])) {
        return true;
    } else {
        return false;
    }

}

function register() {
    $db = new db();

    $checkUsername = check_user_info('username', $_POST['username'] );
    $checkEmail = check_user_info('email', $_POST['email']);

    $registerFeedback = array('checkUsername' => $checkUsername, 'checkEmail' => $checkEmail);

    if (!$checkUsername && !$checkEmail) {
        $db->query("INSERT INTO users(
                              username,
                              firstname,
                              lastname,
                              email,
                              street,
                              housenumber,
                              city,
                              postal,
                              role
                          ) VALUES (
                            :username,
                            AES_ENCRYPT(:firstname, '" . $db->aes . "'),
                            AES_ENCRYPT(:lastname, '" . $db->aes . "'),
                            AES_ENCRYPT(:email, '" . $db->aes . "'),
                            AES_ENCRYPT(:street, '" . $db->aes . "'),
                            AES_ENCRYPT(:housenumber, '" . $db->aes . "'),
                            AES_ENCRYPT(:city, '" . $db->aes . "'),
                            :postal,
                            1
                          )");

        $db->bind(':username', strtolower($_POST['username']));
        $db->bind(':firstname', ucfirst(strtolower($_POST['firstname'])));
        $db->bind(':lastname', $_POST['lastname']);
        $db->bind(':email', strtolower($_POST['email']));
        $db->bind(':street', $_POST['street']);
        $db->bind(':housenumber', $_POST['housenumber']);
        $db->bind(':city', ucfirst(strtolower($_POST['city'])));
        $db->bind(':postal', strtoupper($_POST['postal']));

        try {
            $db->execute();
            $newUserId = $db->lastInsertId();
        } catch (Exception $e) {
            $newUserId = false;
        }

        if ($newUserId) {
            $db->query("INSERT INTO user_pass(user_id, password) VALUES (:userId,:password)");
            $db->bind(':userId', $newUserId);
            $db->bind(':password', hash('sha512', $_POST['password']));
            $db->execute();
            return $newUserId;
        } else {
            $registerFeedback['registerFeedback'] = false;
        }
    }
    return $registerFeedback;
}

function login() {

    //require_once BASEDIR . 'application/inc/db.php';
    if (!check_user_session()) {
        $db = new db();

        $db->query("SELECT
                            user_id,
                            AES_DECRYPT(firstname, '" . $db->aes . "') as firstname,
                            AES_DECRYPT(lastname, '" . $db->aes . "') as lastname,
                            AES_DECRYPT(email, '" . $db->aes . "') as email,
                            AES_DECRYPT(street, '" . $db->aes . "') as street,
                            AES_DECRYPT(housenumber, '" . $db->aes . "') as housenumber,
                            AES_DECRYPT(city, '" . $db->aes . "') as city,
                            postal,
                            role
                          FROM users WHERE username = :username");
        $db->bind(':username', strtolower($_POST['username']));
        $db->execute();
        $userResult = $db->resultset();

        if (empty($userResult)) {
            return false;
        } else {
            $userArr = $userResult[0];

            $db->query('SELECT password FROM user_pass WHERE user_id = :id');
            $db->bind(':id', $userArr['user_id']);
            $db->execute();
            $passResult = $db->single();
            $hashPass = hash('sha512', $_POST['password']);

            if (!empty($passResult) && $passResult['password'] == $hashPass) {
                $userIp = get_user_ip();
                $hash = hash('md5', str_replace('.', '', $userIp) . $userArr['user_id'] . time());
                $db->query("INSERT INTO user_sessions(
                                    user_id,
                                    user_ip,
                                    session_hash,
                                    session_start,
                                    session_end
                                  ) VALUES (
                                    :userId,
                                    :ip,
                                    :hash,
                                    NOW(),
                                    null)");
                $db->bind(':userId', $userArr['user_id']);
                $db->bind(':ip', $userIp);
                $db->bind(':hash', $hash);
                $db->execute();
                $sessionId = $db->lastInsertId();

                if ($sessionId) {
                    $userArr['sessionHash'] = $hash;
                    $_SESSION['user'] = $userArr;

                    if (isset($_POST['remind']) && $_POST['remind'] == 'on') {
                        setcookie('user', $hash, time() + (86400 * 30));
                    }

                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
    } else {
        return 3;
    }

}

function logout() {
    if (isset($_COOKIE['user'])) {
        setcookie('user', null, time()-3600);
    }
    if (isset($_SESSION['user'])) {
        $db = new db();

        $db->query("UPDATE user_sessions SET session_end = NOW() WHERE session_hash = :sessionHash;");
        $db->bind(':sessionHash', $_SESSION['user']['sessionHash']);
        $db->execute();

        session_destroy();

        if (isset($_SESSION['user'])) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }

}

function check_user_session() {

    if (isset($_SESSION['user'])) {
        return $_SESSION['user'];
    } else {
        return false;
    }

}

function check_user_cookie() {

    $db = new db();

    $db->query("SELECT user_id FROM user_sessions WHERE session_hash = :hash AND session_end IS NULL" );
    $db->bind(':hash', $_COOKIE['user']);
    $db->execute();
    $userId = $db->single();

    if ($userId != false) {
        $db->query("SELECT
                            user_id,
                            AES_DECRYPT(firstname, '" . $db->aes . "') as firstname,
                            AES_DECRYPT(lastname, '" . $db->aes . "') as lastname,
                            AES_DECRYPT(email, '" . $db->aes . "') as email,
                            AES_DECRYPT(street, '" . $db->aes . "') as street,
                            AES_DECRYPT(housenumber, '" . $db->aes . "') as housenumber,
                            AES_DECRYPT(city, '" . $db->aes . "') as city,
                            postal,
                            role
                          FROM users WHERE user_id = :userId");
        $db->bind(':userId', (int)$userId['user_id']);
        $db->execute();
        $result = $db->resultset();
        if ($result != false) {
            $userArr = $result[0];
            $userArr['sessionHash'] = $_COOKIE['user'];
            $_SESSION['user'] = $userArr;
        }
    }

}

function get_user_ip() {

    return isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

}
