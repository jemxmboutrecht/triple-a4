<?php


function set_order($userId, $cart, $statusId = 1) {

  //var_dump($cart);exit;
  $check = check_order($userId, $cart['cart_id']);
  $db = new db();

  $orderDate = date('Y-m-d H:i:s',strtotime("+1 hours"));

  if (!$check) {
    $db->query('INSERT INTO orders(
                  user_id,
                  cart_id,
                  order_date,
                  order_price_ex,
                  order_price_inc,
                  order_status_id
                ) VALUES (
                  :userId,
                  :cartId,
                  :orderDate,
                  :priceEx,
                  :priceInc,
                  :statusId
                )');
    $db->bind(':userId', $userId);
    $db->bind(':cartId', (int)$cart['cart_id']);
    $db->bind(':orderDate', $orderDate);
    $db->bind(':priceEx', floatval($cart['carttotal']['ex']));
    $db->bind(':priceEx', floatval($cart['carttotal']['ex']));
    $db->bind(':priceInc', floatval($cart['carttotal']['inc']));
    $db->bind(':statusId', $statusId);
    $db->execute();
    $orderId = $db->lastInsertId();
    if ($orderId) {
      $order = insert_lines($orderId, $cart);
      $order['status'] = 1;
      $order['order_date'] = $orderDate;
      return $order;
    }
  } else {
    $db->query('UPDATE orders SET order_status_id = :statusId WHERE order_id = :orderId');
    $db->bind(':statusId', $statusId);
    $db->bind(':orderId', $check['order_id']);
    $db->execute();
    $check['status'] = $statusId;
    close_cart($cart['cart_id']);
    return $check;
  }
}

function insert_lines($orderId, $cart) {
  $db = new db();
  $orderLineIds = array();
  $i = 1;
  foreach ($cart['products'] as $prdId => $info) {
    $db->query('INSERT INTO `orderlines`(order_id, product_id, amount, discount, price, price_total) VALUES (:orderId,:prdId,:amt,:discount,:prc,:prctotal)');
    $db->bind(':orderId', $orderId);
    $db->bind(':prdId', $prdId);
    $db->bind(':amt', $info['amt']);
    $db->bind(':discount', (isset($info['discount']) ? $info['discount'] : 0));
    $db->bind(':prc', $info['prc']);
    $db->bind(':prctotal', $info['prctotal']);
    $db->execute();
    $orderLineIds[] = $db->lastInsertId();
  }

  return array('order_id' => $orderId, 'orderlines' => $orderLineIds);
}

function check_order($userId, $cartId) {
  $db = new db();

  $db->query('SELECT order_id, order_date, order_status_id FROM orders WHERE user_id = :userId AND cart_id = :cartId');
  $db->bind('userId', $userId);
  $db->bind('cartId', (int)$cartId);

  if ($order = $db->resultset()) {
    $order = $order[0];
    $db->query('SELECT orderline_id FROM orderlines WHERE order_id = :orderId');
    $db->bind(':orderId', $order['order_id']);
    $db->execute();
    $orderlineIds = $db->resultset();

    if ($orderlineIds) {
      $idArr = array();

      foreach ($orderlineIds as $orderline) {
        $idArr[] = (int)$orderline['orderline_id'];
      }

      return array(
        'order_id' => (int)$order['order_id'],
        'order_date' => $order['order_date'],
        'order_status' => $order['order_status_id'],
        'orderlines' => $idArr);

    } else {

      return array(
        'order_id' => (int)$order['order_id'],
        'order_date' => $order['order_date'],
        'order_status' => $order['order_status_id']);

    }

  } else {
    return false;
  }

}

function get_orders($userId) {
  $db = new db();

  $db->query('SELECT * FROM orders WHERE user_id = :userId');
  $db->bind(':userId', $userId);
  $db->execute();
  $orders = $db->resultset();

  if ($orders) {
    return $orders;
  } else {
    return false;
  }
}

function gen_status_label($statusId) {
  $labelStr = '<span class=":classStr"><strong>:statusStr</strong></span>';
  switch ($statusId) {
    case 1:
      $statusStr = 'Open';
      $classStr = 'text-info';
      break;
    case 2:
      $statusStr = 'Succes';
      $classStr = 'text-success';
      break;
    case 3:
      $statusStr = 'Afgebroken';
      $classStr = 'text-danger';
      break;
    case 4:
      $statusStr = 'In de wacht';
      $classStr = 'text-warning';
      break;
  }
  $labelStr = str_replace(':classStr', $classStr, $labelStr);
  $labelStr = str_replace(':statusStr', $statusStr, $labelStr);
  return $labelStr;
}
