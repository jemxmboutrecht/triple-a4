<?php

/*
 * Product functions
 * Author: Jesse Malotaux <jemx@mboutrecht.nl>
 */

function get_products($by = null, $identifier = null) {

    require_once BASEDIR . 'application/inc/db.php';
    $db = new db();

    if (!empty($identifier)) {
        $count = count($identifier);

        for ($i = 0; $i < $count; $i++) {
            if (!isset($paramStr)) {
                $paramStr = ':prm'.$i;
            } else {
                $paramStr .= ',:prm'.$i;
            }
        }
    }

    switch ($by) {
        case 'category':
            $productQuery = 'SELECT * FROM products WHERE category_id IN ('.$paramStr.')';
            break;
        case 'id':
            $productQuery = 'SELECT * FROM products WHERE id IN ('.$paramStr.')';
            break;
        default:
            $productQuery = 'SELECT * FROM products';
            $count = 0;
            break;
    }

    $db->query($productQuery);
    for ($i = 0; $i < $count; $i++) {
        $db->bind(':prm'.$i, $identifier[$i]);
    }
    $db->execute();
    $productResults = $db->resultset();

    return $productResults;

}

function get_products_by_category($categoryArr) {

    $db = new db();

    $count = count($categoryArr);

    for ($i = 0; $i < $count; $i++) {
        if (!isset($paramStr)) {
            $paramStr = ':cat'.$i;
        } else {
            $paramStr .= ',:cat'.$i;
        }
    }

    $db->query('SELECT id FROM product_categories WHERE url IN ('.$paramStr.')');

    for ($i = 0; $i < $count; $i++) {
        $db->bind(':cat'.$i, $categoryArr[$i]);
    }

    $db->execute();
    $categoryResult = $db->resultset();

    $idArr = array();

    foreach ($categoryResult as $category) {
        $idArr[] = $category['id'];
    }

    return get_products('category', $idArr);
}

function get_products_by_id($idArr = null) {

    switch ($idArr) {
        case is_array($idArr):
            $ids = $idArr;
            break;
        case is_string($idArr):
            $ids = explode(',', $idArr);
            break;
        case is_int($idArr):
            $ids = array($idArr);
            break;
    }

    if (empty($ids) && isset($_POST['productIds'])) {
        $ids = $_POST['productIds'];
    }

    return get_products('id', $ids);
}

function get_categories($url = null) {

    $db = new db();

    $db->query('SELECT * FROM product_categories ORDER BY title');
    $db->execute();
    $categoriesResults = $db->resultset();

    $categoryArr = array();

    foreach ($categoriesResults as $category) {
        if (isset($url) && in_array($category['url'], $url)) {
            $category['active'] = true;
        }
        if (empty($category['parent_id'])) {
            if(isset($categoryArr[$category['id']])) {
                $categoryArr[$category['id']] = array_merge($category, $categoryArr[$category['id']]);
            } else {
                $categoryArr[$category['id']] = $category;
            }
        } else {
            $categoryArr[$category['parent_id']]['sub'][$category['id']] = $category;
        }
    }

    return $categoryArr;

}

//function get_products_by_id($ids = null) {
//
//    $idStr = '';
//
//    switch ($ids) {
//        case is_array($ids):
//            $idStr = implode(',', $ids);
//            break;
//        case is_string($ids):
//            $idStr = $ids;
//            break;
//        case is_int($ids):
//            $idStr += $ids;
//            break;
//        case isset($_POST['productIds']);
//            $idStr = implode(',', $_POST['productIds']);
//            break;
//        default:
//            return false;
//            break;
//    }
//
//    require_once BASEDIR . 'application/inc/db.php';
//    $db = new db();
//
//    $query = 'SELECT id, title FROM products WHERE id IN (:ids)';
//    $db->query($query);
//    $db->bind(':ids', $idStr);
//    echo str_replace(':ids', implode(',', $_POST['productIds']), $query);
//    $db->execute();
//    $productResult = $db->resultset();
//
//    print_r($productResult);
//    //print_r($db->resultset());
//}