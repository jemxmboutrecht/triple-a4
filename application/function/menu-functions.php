<?php

/*
 * Menu functions
 * Author: Jesse Malotaux <jemx@mboutrecht.nl>
 */

function get_structure()
{
    $db = new db();

    $pageQuery = $db->query('SELECT * FROM pages ORDER BY page_order');
    $db->execute();
    $pageResult = $db->resultset();

    $pageArr = array();
    $orderArr = array();

    foreach ($pageResult as $page) {
        //print_r($page);
        if (empty($page['parent_id'])) {
            $orderArr[$page['page_order']] = $page['id'];
            $pageArr[$page['id']]['title'] = $page['title'];
            $pageArr[$page['id']]['url'] = $page['page_url'];
        } else {
            $pageArr[$page['parent_id']]['subpages'][$page['id']]['title'] = $page['title'];
            $pageArr[$page['parent_id']]['subpages'][$page['id']]['url'] = $page['page_url'];
        }
    }

    $pageOrderArr = array();

    foreach ($orderArr as $order) {
        $pageOrderArr[] = $pageArr[$order];
    }

    return $pageOrderArr;
}

function gen_link($page, $link) {

    $classStr = 'nav-link';
    $urlStr = $page['url']['baseurl'].$link['url'];
    $adds = '';

    if (isset($link['subpages'])) {
        $classStr .= ' dropdown-toggle';
        $urlStr = '#';
        $adds = 'id="dropdown-'.$link['url'].'" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"';
        $submenuStr = gen_submenu($page, $link['url'], $link['subpages']);
    }
    $linkStr = '<a class="'.$classStr.'" href="'.$urlStr.'" '.$adds.'>'.$link['title'].'</a>';

    if (isset($submenuStr)) {
        $linkStr .= $submenuStr;
    }
    return $linkStr;
}

function gen_submenu($page, $parent, $subpages) {
    $menuStr = '<div class="dropdown-menu" aria-labelledby="dropdown-'.$parent.'">';
    foreach ( $subpages as $sub ) {
        $menuStr .= '<a class="dropdown-item" href="'.$page['url']['baseurl'].$sub['url'].'">'.$sub['title'].'</a>';
    }
    $menuStr .= '</div>';
    return $menuStr;
}
