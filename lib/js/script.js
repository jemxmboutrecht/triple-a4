( function( $ ) {

    cart = load_cart();
    $('button.add-to-cart').on('click', function() {
        var element     = $(this),
            productId   = element.data('productid'),
            price       = element.data('price');
        add_to_cart(productId, price, 1, element);
    });

    var scanVal = '';

    $(window).on('keyup', function(e) {
  		if (e.which >= 48 && e.which <= 57) {
  			scanVal += String.fromCharCode(e.which);
  		}


  		if (e.which == 13) {
        if (window.location.href == 'http://www.triple-a4.nl/shop') {
          var message = 'Geen product met dit SKU, scan een ander artikel.';
        } else {
          var message = 'Geen product met dit SKU, ga naar de Winkel pagina en probeer het nog eens.';
        }
        
        if ($('button[data-sku="'+scanVal+'"]').length > 0) {
          $('button[data-sku="'+scanVal+'"]').trigger('click');
        } else {
          alert(message);
        }
        scanVal = '';

      }
  	});

} )( jQuery );

function load_cart(callback = null) {
    $.ajax({
        url: baseUrl + 'application/function/cart/get_cart_products/',
        method: 'post'
    }).done(function (data) {
        if (typeof data == 'string' && JSON.parse(data)) {
            var returnData = JSON.parse(data);
            if (typeof callback == 'function') {
                callback(returnData);
            } else {
                return returnData;
            }
        }
    });
}

function add_to_cart(productId, price, amount, element) {
    $.ajax({
        url: baseUrl + 'application/function/cart/add/',
        method: 'post',
        data: {productid: productId, price: price, amount: amount}
    }).done(function (data) {
        if (data == '1') {
            succesfull_add(element, productId);
        }
    });
}

function succesfull_add(element, productId) {
    var elId = 'added-'+productId;
    $('<i class="fa fa-check pull-right text-success mr-1" id="'+elId+'" aria-hidden="true"></i>').insertAfter(element);
    $('#'+elId).animate({opacity: 0}, 2000);
}
