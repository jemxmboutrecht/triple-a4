$(document).ready(function() {
    $('form.add-product-form').on('submit', function (e) {
        e.preventDefault();
        var formVals = {};

        $.each($(this).find('input'), function() {
            formVals[$(this).attr('id')] = $(this).val();
        });
        console.log(formVals);
        add_to_cart(formVals['id'], formVals['price'], formVals['amount'], $(this));
    });
});