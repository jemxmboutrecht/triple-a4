$(document).ready(function() {
    $('input.prdamt').on('input', function() {
        var input = $(this),
            oldAmt = input.data('amt'),
            addAmt = parseInt(input.val()) - parseInt(oldAmt);
            prc = input.data('price'),
            row = input.parent('td').parent('tr.prdrow'),
            prdid = row.data('prdid');

        add_to_cart(prdid, prc, addAmt, input);
        update_prd_price(prdid, input.val(), prc);
        update_totals();
        input.data('amt',input.val());
    });

    $('button.delete-product').on('click', function() {
        var button = $(this),
            row = button.parent('td').parent('tr.prdrow'),
            prdid = row.data('prdid'),
            title = row.children('td.prdtitle').html(),
            del = confirm('Are you sure you want to delete "'+title+'" from your shoppingcart?');

        if (del) {
            delete_product(prdid, row);
        }
    });

    $('button#empty-cart').on('click', function() {
        if (confirm('Are you sure you want to empty your cart?')) {
            empty_cart();
        }
    });
});

function update_prd_price(prdid, amt, prc) {
    var newPrice = amt*prc,
        formatPrice = newPrice.toFixed(2),
        strPrice = ''+formatPrice,
        priceArr = strPrice.split('.'),
        priceTxt = priceArr[0]+','+priceArr[1];

    $('tr[data-prdid="'+prdid+'"]').children('td.prdtotal').children('span').html(priceTxt);
}

function update_totals() {

    $('#priceEx, #priceInc').animate({opacity: 0}, function() {
        load_cart(function (data) {
            var products = data['products'],
                priceEx = 0,
                priceInc = 0,
                i = 0;
            console.log(products);
            $.each(products, function() {
                priceEx = priceEx + (this.prc * this.amt);
                i++;
            });

            if (i == 0) {
                location.reload();
            }

            priceInc = parseFloat(priceEx) * 1.21;

            if (typeof priceEx !== 'undefined' || priceInc == 0) {
                priceEx = parseFloat(priceEx).toFixed(2);
                priceInc = priceInc.toFixed(2);

                $('#priceEx').html(priceEx);
                $('#priceInc').html(priceInc);
                $('#priceEx, #priceInc').animate({opacity: 1});
            }
        });
    });
}

function delete_product(prdId, row) {
    $.ajax({
        url: baseUrl + 'application/function/cart/delete/',
        method: 'post',
        data: {productid: prdId}
    }).done(function (data) {
        if (data == '1') {
            row.animate({height: 0, opacity: 0}, function() { $(this).remove() });
            update_totals();
        }
    });
}

function empty_cart() {
  location.reload();
  return;
    $.ajax({
        url: baseUrl + 'application/function/cart/empty_cart/',
        method: 'post'
    }).done(function (data) {
        console.log(data);
        if (data == '1') {
            location.reload();
        }
    });
}
